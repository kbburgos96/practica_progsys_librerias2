hash: libsha1.a main.c
	gcc -c main.c
	gcc -static -o hash main.c libsha1.a

hash_dyn: main.c
	gcc -o hash_dyn main.c libsha1.so -L. libsha1.so -Wl,-rpath=.
